<?php

/**
 * Show insert posts button in Dashboard
 */
add_action("admin_notices", function () {
    $screen = get_current_screen();
    if ('localizacao' == $screen->post_type) {
        echo "<div class='notice notice-info'>";
        echo "<p>";
        echo "To insert the posts into the database, click the button to the right.";
        echo "<a class='button button-primary' style='margin:0.25em 1em' href='{$_SERVER["REQUEST_URI"]}&insert_csv_posts'>Insert Posts</a>";
        echo "</p>";
        echo "</div>";
    }
});

/**
 * Create and insert posts from CSV files
 */
add_action("admin_init", function () {
    global $wpdb;

    // I'd recommend replacing this with your own code to make sure
    // the post creation _only_ happens when you want it to.
    if (!isset($_GET["insert_csv_posts"])) {
        return;
    }

    // Change these to whatever you set
    $csv = array(
        "custom-taxonomy"       => "category",
        "custom-post-type"      => "location"
    );

    // Get the data from all those CSVs!
    $posts = function () {
        $data = array();
        $errors = array();

        // Get array of CSV files
        $files = glob(__DIR__ . "*/data/*.csv");

        foreach ($files as $file) {

            // Attempt to change permissions if not readable
            if (!is_readable($file)) {
                chmod($file, 0744);
            }

            // Check if file is writable, then open it in 'read only' mode
            if (is_readable($file) && $_file = fopen($file, "r")) {

                // To sum this part up, all it really does is go row by
                //  row, column by column, saving all the data
                $post = array();

                // Get first row in CSV, which is of course the headers
                $header = fgetcsv($_file);

                while ($row = fgetcsv($_file)) {

                    foreach ($header as $i => $key) {
                        $post[$key] = $row[$i];
                    }

                    $data[] = $post;
                }

                fclose($_file);
            } else {
                $errors[] = "File '$file' could not be opened. Check the file's permissions to make sure it's readable by your server.";
            }
        }

        if (!empty($errors)) {
            return;
        }

        return $data;
    };

    // Simple check to see if the current post exists within the
    // database. This isn't very efficient, but it works.
    $post_exists = function ($title) use ($wpdb, $csv) {

        // Get an array of all posts within our custom post type
        $posts = $wpdb->get_col("SELECT post_title FROM {$wpdb->posts} WHERE post_type = '{$csv["custom-post-type"]}'");

        // Check if the passed title exists in array
        return in_array($title, $posts);
    };

    foreach ($posts() as $post) {

        // If the post exists, skip this post and go to the next one
        if ($post_exists($post["customer"])) {
            echo "<div class='notice notice-info'>";
            echo "<p>";
            echo "Posts already " . $post["customer"] . " Inserted";
            echo "</p>";
            echo "</div>";
            continue;
        }

        // Insert the post into the database
        $post["id"] = wp_insert_post(array(
            "post_title"    => $post["customer"],
            'post_category' => array($post["category"]),
            "post_content"  => $post["description"],
            "post_type"     => $csv["custom-post-type"],
            "post_status"   => "publish",
            'meta_input' => array(
                'custom_fields_id'      => $post['id'],
                'custom_fields_address' => $post['address']
            ),
            'tax_input' => array(
                'category' => array(
                    $post["category"]
                )
            )

        ));
    }
});
